# How to create the Raspberry Pi img file with auto resize

- Install the system using the process described in the README
- If required, set the desired refresh rate and resolution: 1920x1080 60Hz for Nec Desplays 
- Install the player without reboot the system
- Remove the downloaded installer to free up some space.
- Add the following parameter: `init=/usr/lib/raspi-config/init_resize.sh` to `/boot/cmdline.txt`
- Add `resize2fs_once` to the `/etc/init.d/` folder.
```
#!/bin/sh
### BEGIN INIT INFO
# Provides:          resize2fs_once
# Required-Start:
# Required-Stop:
# Default-Start: 3
# Default-Stop:
# Short-Description: Resize the root filesystem to fill partition
# Description:
### END INIT INFO
. /lib/lsb/init-functions
case "$1" in
  start)
    log_daemon_msg "Starting resize2fs_once"
    ROOT_DEV=$(findmnt / -o source -n) &&
    resize2fs $ROOT_DEV &&
    update-rc.d resize2fs_once remove &&
    rm /etc/init.d/resize2fs_once &&
    log_end_msg $?
    ;;
  *)
    echo "Usage: $0 start" >&2
    exit 3
    ;;
esac
```
- Add the execution permissions: `chmod +x /etc/init.d/resize2fs_once`.
- Update the startup system to be executed: `update-rc.d resize2fs_once defaults`.
- Delete unnecessary files apt cache: `apt-get clean`.
- Add versions into: `admira-ald_version 0.8 ARM-AYY.MM.DD YYYY-MM-DD-raspios-bullseye-armhf-lite.img.xz >> /etc/admira-ald_version`.
- Forget wifi network to avoid password leaks
- Halt the system.
- Perform the img with shrink option enabled using the `ApplePiBakker.app`
- Then we compress the img file: `xz admira-ald-v0.7_ARM-A23.03.24.img`
