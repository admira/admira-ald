#!/bin/bash

# Run as root or insert `sudo -E` before `bash`:
#
# wget -qO - https://bitbucket.org/admira/admira-ald/raw/master/setup | bash -
#
# If you want to test a different branch execute the following commands:
#
# wget https://bitbucket.org/admira/admira-ald/raw/AP-XXXX/setup
# chmod a+x setup
# sudo BASE_URL="https://bitbucket.org/admira/admira-ald/raw/AP-XXXX/" ./setup
#

BASE_URL="${BASE_URL:-https://bitbucket.org/admira/admira-ald/raw/master/}"

# This script should be running as root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

function add_nonfree_sources {
  echo -e "======> \e[92mAdding non-free apt sources.\e[39m\n"
  sed /etc/apt/sources.list -i -e "s/main$/main non-free/" 
}

function update_index_pkgs {
  echo -e "======> \e[92mResynchronizing Index Packages From Sources.\e[39m\n"
  apt-get update
}

function create_user {
  PASSWORD=$(whiptail --passwordbox "The admira user is going to be created. Please, type the desired password for the new user." 8 70 --title "Admira User Password." 3>&1 1>&2 2>&3)
  if [ $? = 0 ]
  then
    # Add Admira User
    /usr/sbin/useradd -m -p $(mkpasswd $PASSWORD) -s /bin/bash admira
    /usr/sbin/usermod -a -G cdrom,floppy,audio,dip,video,plugdev,netdev,sudo,crontab admira
    sed -i "/^%sudo/d/" /etc/sudoers
    echo "%sudo   ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
    
    # Disable Display Power Management Signaling (DPMS) and prevent screen saver from blanking    
    test -e /home/admira/.xsessionrc || echo  "xset s off -dpms" > /home/admira/.xsessionrc
    echo "PATH=$PATH:/usr/sbin" >> /home/admira/.xsessionrc
    chown admira.admira /home/admira/.xsessionrc
  else
    echo "User selected Cancel"
  fi
}

function base_packages_installation {
case $1 in
  8.*)
     echo -e "======> \e[92mInstalling Required Packages for Debian 8.x based distro.\e[39m\n"
     apt-get -y install xcompmgr \
                        libgles2-mesa \
                        libgl1-mesa-dri \
                        x11-xserver-utils \
                        unzip bzip2 sudo \
                        lightdm \
                        libgnome-keyring0 \
                        libnspr4-0d \
                        libgtk2.0-0 \
                        libnss3-1d \
                        libnss-mdns \
                        libxslt1.1 \
                        libcanberra-gtk-module \
                        gtk2-engines-murrine \
                        zenity fluxbox fbautostart \
                        xorg \
                        x11vnc \
                        network-manager-gnome
     ;;
  9.*)
    echo -e "======> \e[92mInstalling free and non-free firmware.\e[39m\n"
    apt-get -y install firmware-linux \
                       firmware-misc-nonfree \
                       firmware-iwlwifi \
                       firmware-realtek
    echo -e "======> \e[92mInstalling Required Packages for Debian 9.x based distro.\e[39m\n"
    apt-get -y install xcompmgr \
                       libgles2-mesa \
                       libgl1-mesa-dri \
                       x11-xserver-utils \
                       unzip bzip2 sudo whois \
                       lightdm \
                       libgnome-keyring0 \
                       libnspr4 \
                       libgtk2.0-0 \
                       libnss3 \
                       libnss-mdns \
                       libxslt1.1 \
                       libcanberra-gtk-module \
                       gtk2-engines-murrine \
                       zenity fluxbox fbautostart \
                       xorg \
                       x11vnc \
                       scrot \
                       network-manager-gnome \
                       pulseaudio \
                       pavucontrol \
                       psmisc \
                       mousepad
    echo -e "======> \e[92mInstalling Debugging tools.\e[39m\n"
    apt-get -y install curl \
                       tcpflow-nox \
                       pamix
    ;;
  10|10.x)
    echo -e "======> \e[92mInstalling free and non-free firmware.\e[39m\n"
    apt-get -y install firmware-linux \
                       firmware-misc-nonfree \
                       firmware-iwlwifi \
                       firmware-realtek
    # libgnome-keyring0 removed from debian10: https://lists.debian.org/debian-devel/2018/02/msg00169.html use libsecret instead
    # libgnome-keyring0 was a requirement for the Adobe Air's ADmira Player version
    # Debian 10 only allow to install the HTML5 version
    echo -e "======> \e[92mInstalling Required Packages for Debian 10.x based distro.\e[39m\n"
    apt-get -y install xcompmgr \
                       libgles2-mesa \
                       libgl1-mesa-dri \
                       x11-xserver-utils \
                       unzip bzip2 sudo whois \
                       lightdm \
                       libnspr4 \
                       libgtk2.0-0 \
                       libnss3 \
                       libnss-mdns \
                       libxslt1.1 \
                       libcanberra-gtk-module \
                       gtk2-engines-murrine \
                       zenity fluxbox fbautostart \
                       xorg \
                       x11vnc \
                       network-manager-gnome \
                       libgconf-2-4 \
                       scrot \
                       pulseaudio \
                       pavucontrol \
                       psmisc \
                       mousepad \
                       onboard
    echo -e "======> \e[92mInstalling Debugging tools.\e[39m\n"
    apt-get -y install curl \
                       tcpflow-nox \
                       mesa-utils \
                       pamix
    ;;
  11|11.x)
    echo -e "======> \e[92mInstalling free and non-free firmware.\e[39m\n"
    apt-get -y install firmware-linux \
                       firmware-misc-nonfree \
                       firmware-iwlwifi \
                       firmware-realtek
    # Debian 11 only allow to install the HTML5 version
    # Simplemente se ha copiado la instalacion de Debian 10
    echo -e "======> \e[92mInstalling Required Packages for Debian 11.x based distro.\e[39m\n"
    touch /opt/eula.accept
    apt-get -y install xcompmgr \
                       libgles2-mesa \
                       libgl1-mesa-dri \
                       x11-xserver-utils \
                       unzip bzip2 sudo whois \
                       lightdm \
                       libnspr4 \
                       libgtk2.0-0 \
                       libnss3 \
                       libnss-mdns \
                       libxslt1.1 \
                       libcanberra-gtk-module \
                       gtk2-engines-murrine \
                       zenity fluxbox fbautostart \
                       xorg \
                       x11vnc \
                       network-manager-gnome \
                       libgconf-2-4 \
                       scrot \
                       pulseaudio \
                       pavucontrol \
                       psmisc \
                       mousepad \
                       xterm \
                       onboard
    echo -e "======> \e[92mInstalling Debugging tools.\e[39m\n"
    apt-get -y install curl \
                       tcpflow-nox \
                       mesa-utils \
                       pamix
    ;;
  *)
   echo -e "======> \e[92mbase_packages_installation (ERROR) - Unknown argument passed: $1\e[39m\n"
      ;;
esac
}

function lightdm_setup {
  echo -e "======> \e[92mSetting lightdm.conf.\e[39m\n"
  LIGHTDMCONF="/etc/lightdm/lightdm.conf"
  GREETERCONF="/etc/lightdm/lightdm-gtk-greeter.conf"
  # Parsing for Raspbian
  sed $LIGHTDMCONF -i -e "s/^autologin-user=pi/autologin-user=admira/"
  sed $LIGHTDMCONF -i -e "s/^#autologin-user-timeout=60/autologin-user-timeout=0/"
  # Parsing for Debian
  sed $LIGHTDMCONF -i -e "s/^#autologin-user=/autologin-user=admira/"
  sed $LIGHTDMCONF -i -e "s/^#autologin-user-timeout=0/autologin-user-timeout=0/"
  mkdir -p /usr/share/images/desktop-base
  wget -qO /usr/share/images/desktop-base/admira-background.png $BASE_URL/lightdm-theme/background.png
  #update-alternatives --install /usr/share/images/desktop-base/desktop-background desktop-background /usr/share/images/desktop-base/admira-background.png 65
  #update-alternatives --set desktop-background /usr/share/images/desktop-base/admira-background.png
  sed $GREETERCONF -i -e "s/^#background\b.*$/background=\/usr\/share\/images\/desktop-base\/admira-background.png/"
  
  if ([ $(lsb_release -si) == "Raspbian" ] || [ $(lsb_release -si) == "Debian" ]) && [ $(lsb_release -sr) == 11 ];
  then
    # Force LigthDM startup at boot time in Raspberry Pi Os (Bullseye)
    systemctl set-default graphical.target
  fi
}

function grub_setup {
 echo -e "======> \e[92mGRUB background setup.\e[39m\n"
 wget -qO /usr/share/images/desktop-base/admira-grub-background.png $BASE_URL/grub/background.png
 #update-alternatives --install /usr/share/images/desktop-base/desktop-grub desktop-grub /usr/share/images/desktop-base/admira-grub-background.png 65
 #update-alternatives --set desktop-grub /usr/share/images/desktop-base/admira-grub-background.png
 grep -q GRUB_BACKGROUND /etc/default/grub || echo "GRUB_BACKGROUND=/usr/share/images/desktop-base/admira-grub-background.png" >> /etc/default/grub
 /usr/sbin/update-grub
}

function fluxbox_setup {
  
  case $(arch) in
    'armv7l'|'aarch64')
      bundle_arch='ARM'
    ;;
    'i686')
      bundle_arch='x32'
    ;;
    'x86_64')
      bundle_arch='x64'
    ;;
    '*')
    ;;
  esac
  
  if [ ! -d /home/admira/.fluxbox ]
  then
    echo -e "======> \e[92mSetting Fluxbox look & feel.\e[39m\n"
    mkdir -p /home/admira/.fluxbox/styles/admira
    mkdir /home/admira/.fluxbox/backgrounds
    mkdir /home/admira/.fluxbox/icons
    mkdir /home/admira/tools
    chown admira.admira /home/admira/tools
    echo -en "      > \e[92mGetting files.\e[39m"
    for file in apps init keys lastwallpaper rasp-menu rasp-menu-final debian-menu debian-menu-final overlay slitlist startup windowmenu
    do
      echo -n '.'
      wget -qO /home/admira/.fluxbox/$file $BASE_URL/fluxbox/$file
    done
    echo -e "\n      > \e[92mRetrieving available bundle versions for the $bundle_arch architecture.\e[39m"
    versions=$(wget -qO- http://install.admira.com/linux/list| sed -n '/^$/!{s/<[^>]*>//g;p;}'| sed 's/ - /\n/g' | grep $bundle_arch)
    for version in $versions
      do
        menu_entries="$menu_entries             [exec] (Install ver ($version\\\)) {x-terminal-emulator -T \"ADmira Player Installer - HTML5 ($version)\" -e /home/admira/install $version} </home/admira/.fluxbox/icons/admira.png>\n"
      done
    sed -i "/Generated at runtime by the setup script/i \             # Generated at runtime by the setup script\n$menu_entries" /home/admira/.fluxbox/$1-menu
    mv /home/admira/.fluxbox/$1-menu /home/admira/.fluxbox/menu
    echo -en "      > \e[92mGetting backgrounds.\e[39m"
    for file in 'Admira Green' 'Admira Logo' 'Dark' 'Dark Green' 'Dark Grey' 'Dark Pink' 'Light Green' 'Light Grey' 'Orange'
    do
      echo -n  '.'
      wget -qO "/home/admira/.fluxbox/backgrounds/$file" "$BASE_URL/fluxbox/backgrounds/$file"
    done
    wget -qO /home/admira/.fluxbox/styles/admira/theme.cfg $BASE_URL/fluxbox/styles/admira/theme.cfg
    echo -en "\n      > \e[92mGetting custom icons.\e[39m"
    for file in edit.png bugreport.png folder.png playlist.png video_library.png network.png alarm.png applications.png audio.png admira.png logout.png restart.png rotate_right.png shutdown.png display_orientation.png display_resolution.png normal.png rotate_left.png settings.png terminal.png color.png wallpaper.png keyboard.png raspberry-96.png
    do
      echo -n '.'
      wget -qO /home/admira/.fluxbox/icons/$file $BASE_URL/fluxbox/icons/$file
    done
    wget -qO /home/admira/install $BASE_URL/install
    wget -qO /home/admira/tools/teamviewer $BASE_URL/tools/teamviewer
    wget -qO /home/admira/tools/shutdown-scheduler.sh $BASE_URL/tools/shutdown-scheduler.sh
    wget -qO /home/admira/tools/screen-manager.sh $BASE_URL/tools/screen-manager.sh
    wget -qO /home/admira/tools/screen_orientation $BASE_URL/tools/screen_orientation
    wget -qO /home/admira/tools/update-resolutions-menu.sh $BASE_URL/tools/update-resolutions-menu.sh
    wget -qO /home/admira/tools/admira-ald_version $BASE_URL/tools/admira-ald_version
    chmod a+x /home/admira/install
    chown admira.admira /home/admira/install
    chmod a+x /home/admira/tools/teamviewer
    chown admira.admira /home/admira/tools/teamviewer
    chmod a+x /home/admira/tools/shutdown-scheduler.sh
    chown admira.admira /home/admira/tools/shutdown-scheduler.sh
    chmod a+x /home/admira/tools/screen-manager.sh
    chown admira.admira /home/admira/tools/screen-manager.sh
    ln -s /home/admira/tools/screen-manager.sh /usr/bin/screen-manager.sh
    chown admira.admira /home/admira/tools/screen_orientation
    chown admira.admira /home/admira/tools/update-resolutions-menu.sh
    chmod a+x /home/admira/tools/update-resolutions-menu.sh
    chmod a+x /home/admira/tools/admira-ald_version
    chown -R admira.admira /home/admira/.fluxbox
    # xterm font size is too small by default so we set a bigger one
    echo "xterm*font:     *-fixed-*-*-*-18-*" > /home/admira/.Xresources
    chown admira.admira /home/admira/.Xresources
    echo ""
  else
    echo -e "======> \e[93mFluxbox look & feel already setted\e[39m\n"
  fi
}

function network_setup {
  echo -e "\n======> \e[92mRemoving devices from interfaces file in order to allow network-manager to use them.\e[39m\n"
  sed -i '/auto lo$/,$d' /etc/network/interfaces
  if ([ $(lsb_release -si) == "Raspbian" ] || [ $(lsb_release -si) == "Debian" ]) && [ $(lsb_release -sr) == 11 ];
  then
    systemctl enable NetworkManager
  fi
}

function rpi_set_config_var {
  # Taken from https://github.com/RPi-Distro/raspi-config
  lua - "$1" "$2" "$3" <<EOF > "$3.bak"
local key=assert(arg[1])
local value=assert(arg[2])
local fn=assert(arg[3])
local file=assert(io.open(fn))
local made_change=false
for line in file:lines() do
  if line:match("^#?%s*"..key.."=.*$") then
    line=key.."="..value
    made_change=true
  end
  print(line)
end
if not made_change then
  print(key.."="..value)
end
EOF
mv "$3.bak" "$3"
}

#####################################################
# edit_ini_file                                     #
# Edit an INI file. Either edit existing or add new.#
# Params: $1 = Section to find                      #
#         $2 = Key to find in section               #
#         $3 = Replacement/addition Line            #
#         $4 = Full Path to ini File                #
# Return: None                                      #
# Note:  This either edits an existing line or adds #
#        a new line in the given section.           #
#####################################################
function rpi_edit_ini_file {
  # Taken from https://github.com/SharpNECDisplaySolutions/nec_rpi_config_tool
  sudo lua - "$1" "$2" "$3" "$4"<<EOF > /tmp/editini.txt
  local section=assert(arg[1])
  local key=assert(arg[2])
  local line_value=assert(arg[3])
  local fn=assert(arg[4])
  local file=assert(io.open(fn))
  local found_section=false
  local change_made=false
  
  for line in file:lines() do
    if line:match("^%["..section.."%]$") then
      found_section=true
    elseif found_section then
      if line:match("^#?%s*"..key.."=.*$") then
        change_made=true
        line=line_value
      elseif line:match("^%[.*%]$") then
        if not change_made then
           print(line_value)
        end
        found_section=false
      end
    end
    print(line)
  end
EOF
  sudo cp "/tmp/editini.txt" "$4"
  rm /tmp/editini.txt 
}

function rpi_clear_config_var {
  # Taken from https://github.com/RPi-Distro/raspi-config
  lua - "$1" "$2" <<EOF > "$2.bak"
local key=assert(arg[1])
local fn=assert(arg[2])
local file=assert(io.open(fn))
for line in file:lines() do
  if line:match("^%s*"..key.."=.*$") then
    line="#"..line
  end
  print(line)
end
EOF
mv "$2.bak" "$2"
}

function rpi_get_config_var {
  # Taken from https://github.com/RPi-Distro/raspi-config
  lua - "$1" "$2" <<EOF
local key=assert(arg[1])
local fn=assert(arg[2])
local file=assert(io.open(fn))
local found=false
for line in file:lines() do
  local val = line:match("^%s*"..key.."=(.*)$")
  if (val ~= nil) then
    print(val)
    found=true
    break
  end
end
if not found then
   print(0)
end
EOF
}

function rpi_config {
  board=$(cat /proc/cpuinfo | grep Model| cut -d':' -f 2|sed 's/^\ //')
  soc=$(cat /proc/cpuinfo | grep Hardware| cut -d':' -f 2|sed 's/^\ //')
  echo -e "======> \e[92mRaspberry Pi Board: $board\e[39m\n"
  echo -e "======> \e[92mRaspberry Pi SoC: $soc\e[39m\n"
  CONFIG="/boot/config.txt"

  if [ ! -e /boot/overlays/vc4-kms-v3d.dtbo ]
  then
    echo -e "======> \e[91mDriver and kernel not present on your system. Please update to Raspbian Lite (jessie) or higher.\e[39m\n"
    exit 1
  fi

  if grep -q -E "^dtoverlay=vc4-kms-v3d|^dtoverlay=vc4-fkms-v3d" $CONFIG
  then
    echo -e "======> \e[93mGL driver for desktop already setted.\e[39m\n"
  else
    echo -e "======> \e[92mSetting /boot/config.txt, xorg.conf.d to enable GL driver for desktop.\e[39m\n"
    sed $CONFIG -i -e "s/^#dtoverlay=vc4-kms-v3d/dtoverlay=vc4-kms-v3d/"
    if ! grep -q -E "^dtoverlay=vc4-kms-v3d" $CONFIG
    then
      printf "dtoverlay=vc4-kms-v3d\n" >> $CONFIG
    fi
    mkdir -p /etc/xdg/autostart
    cat > /etc/xdg/autostart/xcompmgr.desktop <<EOF
[Desktop Entry]
Type=Application
Name=xcompmgr
Comment=Start simple compositor
NoDisplay=true
Exec=xcompmgr -a
EOF
    mv /usr/share/X11/xorg.conf.d/99-fbturbo.conf /usr/share/X11/xorg.conf.d/99-fbturbo.~
    sed $CONFIG -i -e "s/^gpu_mem/#gpu_mem/"
  fi
  
  # BCM2711 -> Raspberry Pi 4 Model B, Raspberry Pi 400, and the Raspberry Pi Compute Module 4.
  # https://www.raspberrypi.com/documentation/computers/processors.html
  #if [ "$soc" == "BCM2711" ]
  #then
  #  
  #fi
  
  # We'll assume that all the Compute Modules 4 detected are NEC MultiSync M491 Diplays
  if [ "$board" == "Raspberry Pi Compute Module 4 Rev 1.0" ]
  then
    echo -e "======> \e[92mA Raspberry Pi Compute Module 4 has been detected so we'll apply custom settings for NEC Displays..\e[39m\n"
    # HDMI settings
    rpi_set_config_var hdmi_group 1 $CONFIG
    rpi_set_config_var hdmi_mode 16 $CONFIG
    rpi_set_config_var hdmi_force_hotplug 1 $CONFIG
    rpi_set_config_var disable_overscan 1 $CONFIG
    # [pi4]
    # Run as fast as firmware / board allows
    rpi_edit_ini_file pi4 arm_boost 'arm_boost=1' $CONFIG
    # [cm4]
    # Enable host mode on the 2711 built-in XHCI USB controller.
    rpi_edit_ini_file cm4 otg_mode 'otg_mode=1' $CONFIG
    # Configure GPIO for shutdown and display fan control
    rpi_edit_ini_file cm4 dtoverlay 'dtoverlay=gpio-shutdown,gpio_pin=23' $CONFIG
    # Workarround cause duplicated keys are not supported by the function
    sed $CONFIG -i -e "s/^dtoverlay=gpio-shutdown,gpio_pin=23/dtoverlay=gpio-shutdown,gpio_pin=23\ndtoverlay=gpio-fan,gpiopin=24,temp=80000/"
    # Instructs CM4 to use external Wi-Fi antenna instead of the built-in antenna
    rpi_edit_ini_file cm4 dtparam 'dtparam=ant2' $CONFIG 
    # [all]
    rpi_edit_ini_file all gpu_mem 'gpu_mem=128' $CONFIG
    rpi_edit_ini_file all dtoverlay 'dtoverlay=disable-bt' $CONFIG
    # Workarround cause duplicated keys are not supported by the function
    sed $CONFIG -i -e "s/^dtoverlay=disable-bt/dtoverlay=disable-bt\ndtoverlay=uart1/"
    rpi_edit_ini_file all enable_uart 'enable_uart=1' $CONFIG
    
    echo -e "======> \e[92mInstalling NEC Python PD SDK which provides APIs for communicating with the host display.\e[39m\n"
    apt-get install -y python3-pip
    pip3 install nec_pd_sdk
  fi
}

function final_message {
  echo -e "        \e[92mReboot the system and execute the install\e[39m"
  echo -e "        \e[92mApplications->ADmira Player->Install\e[39m"
  echo -e "        \e[92mType: sudo reboot\e[39m"
}

# Architecture and Linux Distribution detection

os=$(lsb_release -si)
ver=$(lsb_release -sr)
case $(arch) in
'armv7l'|'aarch64')
  if ([ $os != "Raspbian" ] && [ $os != "Debian" ]) || ! [[ $ver =~ ^[8|9|10|11]* ]];
  then
    echo -e "\e[91mERROR! Your Linux distribution \"$(lsb_release -sd)\" is not supported. Use Raspbian or Rasperry Pi OS Lite and try again.\e[39m\n"
    exit 1
  fi
  if [ $(arch) == 'aarch64' ] 
  then
    echo -e "======> \e[93mINFO: Kernel: 64-bit \e[39m\n"
  else
    echo -e "======> \e[92mINFO: Kernel: 32-bit \e[39m\n"
  fi
  if [ $(file -Lb /sbin/init | awk '{ print $2 }') == '32-bit' ] 
  then
    echo -e "======> \e[92mINFO: Userland: 32-bit \e[39m\n"
  else
    echo -e "\e[91mERROR: Userland: 64-bit \e[39m\n"
    echo -e "\e[91mERROR! The 64 bits ARM architecture is not supported yet. Use the 32 bits OS version and try again.\e[39m\n"
    exit 1
  fi
  # Install arch required pkgs
  update_index_pkgs
  apt-get install -y xserver-xorg-video-fbturbo
  apt-get install -y xserver-xorg-legacy
  apt-get install -y rpi-chromium-mods chromium-browser chromium-browser-l10n chromium-codecs-ffmpeg-extra
  apt-get install -y gldriver-test libgl1-mesa-dri
  echo -e "======> \e[92mInstalling Debugging tools.\e[39m\n"
  apt-get install -y curl 
  apt-get install -y tcpflow-nox
  apt-get install -y pamix

  base_packages_installation $ver
  create_user
  lightdm_setup
  fluxbox_setup rasp    
  network_setup
  rpi_config
  final_message
  ;;
'i686'|'x86_64')
  if [ $(lsb_release -si) != "Debian" ] || ! [[ $ver =~ ^[8|9|10|11]* ]]
  then
    echo -e "\e[91mERROR! Your Linux distribution \"$(lsb_release -sd)\" is not supported. Use Debian (8, 9, 10 or 11) and try again.\e[39m\n"
    exit 1
  fi
  add_nonfree_sources
  update_index_pkgs
  apt-get -y install sudo whois
  apt-get -y install chromium chromium-l10n chromium-common
  create_user
  base_packages_installation $ver
  lightdm_setup
  grub_setup
  fluxbox_setup debian
  network_setup
  final_message
  ;;
*)
  echo -e "\e[91mERROR! Incorrect arch detected. This installer is for RaspberryPi 3 or 4 (armv7l) and i686 arhcitectures only.\e[39m\n"
  ;;
esac
