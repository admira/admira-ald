# Admira Linux Distribution (ALD) #

This `setup` tool allows you to install the Admira Player in a Debian based distribution, including the Rasperry Pi OS Lite.

We support Debian and Raspberry Pi OS Linux distributions. The `setup` script just requires a base operative system installation, without desktop. The script takes this basic installation as a starting point to build our custom Debian version focused on our Digital Signage solution.

When the installation process ends, the system automatically starts a customized and lightweight desktop environment containing just the required packages to install and run the Admira Digital Signage Player.

The following is a capture of how the Admira Linux Desktop looks like: [Admira Linux Desktop Video Capture](https://youtu.be/Vm7JIYbjTtk)

![ALD Desktop](fluxbox/ald-desktop.png)

## Installation Methods Available

### Pre-Installed Images (Raspberry Pi only)

If you are planning to use a Raspberry Pi, the easiest way to install the Admira Linux Distribution is using the **Raspberry Pi Imager** with one of our custom images. We have the following flavours:

  * [`admira-ald-v0.8_ARM-A23.05.09.img.xz`](https://install.admira.com/firmwares/raspberrypi/armv7l/admira-ald-v0.8_ARM-23.05.09.img.xz): Generic image with the Admira Player already installed. Ready to be used with all the supported Rasperry Pi boards. Just start and register the Admira Player.
  * [`admira-ald-v0.8.img.xz`](https://install.admira.com/firmwares/raspberrypi/armv7l/admira-ald-v0.8.img.xz): Generic image without the Admira Player installed. Ready to be used with all the supported Raspberry Pi boards. You can install any Admira Player Available from the **Admira Player Installer** drop down list menu.
  * [`admira-ald-nec-v0.8_ARM-A23.05.09.img.xz`](https://install.admira.com/firmwares/raspberrypi/armv7l/admira-ald-nec-v0.8_ARM-A23.05.09.img.xz): Specific image to be used with the Raspberry Pi Compute Module 4 included in the Digital Signage NEC displays. This version comes with the Admira Player already installed and ready to use.
  * [`admira-ald-nec-v0.8.img.xz`](https://install.admira.com/firmwares/raspberrypi/armv7l/admira-ald-nec-v0.8.img.xz): This is the same image version but comes without the Admira Player installed so you can choose the desired version you want to install from the **Admira Player Installer** drop down list menu.

If you choose an image with the Player already Installed, you will see then the Admira Player registration form. Provide your Admira Player credentials to register the player in our Digital Signage platform.

Otherwise, you will be logged-in into the Admira Linux Desktop. Just Right-click the left mouse button to see the Admira Options Menu. Go to the Admira Player Installation section in order to know how to install our Digital Signage Player Software.

### setup Script (Debian and Raspberry Pi OS)

If you don't wan to install using our Raspberry Pi pre-configured images or you are not using a Raspberry Pi board, then you need to install one of the supported Debian based Linux distributions, and then just run the `setup` script as shown in the [How to build the Admira Linux Distribution](#markdown-header-How-to-build-the-Admira-Linux-Distribution) section to know how to proceed.

### Requirements

Following you will find the list of all the supported hardware and the supported Debian based distributions:

  * **Supported Hardware**
    * i686
    * x86_64
    * armv7l and aarch64 (including the following boards):
          * RaspberryPi 3
          * RaspberryPi 3 Model B+
          * RasperryPi 4
          * RaspberryPI Zero 2 W
          * Raspberry Pi Compute Module 4 (including NEC Displays)
  * **Supported Distros**
    * Debian
          * 8.x (Jessie)
          * 9.x (Stretch)
          * 10.x (Buster)
          * 11.x (Bullseye
    * Raspbian Lite
          * Jessie
          * Stretch
          * Buster
    * Raspberry Pi OS Lite
          * Buster
          * Bullseye

**Note**: With the exception of Raspberry PI Zero 2 W, all the supported Raspberry Pi boards can handle FULL HD screen resolutions fluently.  

### How to install the admira-ald Raspberry Pi image

The installation process using one of our custom images goes like this:

First, choose one of our custom image files based on your needs. Then, open the Raspberry Pi Imager:

![Raspberry Pi Imager](docs/img/raspberry_pi_imager_1.png)

Click on the CHOOSE OS button and select the Use Custom image option:

![Choose custom image option](docs/img/raspberry_pi_imager_2.png)

Now click on the CHOOSE STORAGE button:

![Choose custom image option](docs/img/raspberry_pi_imager_3.png)

Then Select you device:

![Select the device](docs/img/raspberry_pi_imager_4.png)

Now you can push the WRITE button to start the proccess:

![Choose custom image option](docs/img/raspberry_pi_imager_5.png)

The following warning message will be showed. Press YES:

![Choose custom image option](docs/img/raspberry_pi_imager_6.png)

The copy starts and a progress bar will showed:

![Installation progress](docs/img/raspberry_pi_imager_7.png)

A final message will be showed. Now you can remove the SD Card from the from the card reader.

![Installation done](docs/img/raspberry_pi_imager_8.png)


### How to build the Admira Linux Distribution

Assuming you have a Debian or Raspberry Pi OS already installed, follow the steps below in order to install the Admira Player in your computer.

**Note**: If you don't know how to install Debian, or the Raspberry Pi OS, go to the sections: **Install Debian** or **Install Raspberry Pi OS Lite ** respectively at the end of the document.

  * If you are using the default Raspbian and/or Raspberry Pi OS installation, log into the system using the `pi` user and the password `raspberry` otherwise login using the user created with the Raspberry Pi Imager. If you are in Debian, then log into the system with the `root` user created at installation time. Then execute the following command:
    * **In Raspberry Pi OS**: `wget -qO - https://bitbucket.org/admira/admira-ald/raw/master/setup | sudo -E bash -`
    * **In Debian**:   `wget -qO - https://bitbucket.org/admira/admira-ald/raw/master/setup | bash -`
  * Set a password for the admira user when prompted.
  * When the script ends, just reboot the system with this command: `sudo reboot` 
  * The system will start a desktop environment.
  * Right-click with the mouse and select: *Applications -> ADmira Player -> Install*
  * Follow the instructions and restart the system.
  * The system will start the ADmira Player register screen.
  * Register the player in our platform. Look at the section "How to register the player in the admira platform" for further steps.

### How to install the Admira Player

In this section will explain you how to install the Admira Digital Signage Player on your Admira Linux Distribution.

First right-click the mouse button to see the main **admira** Desktop Menu whose looks like this:

![Main menu](docs/img/main-menu.png)

Then click over the **Admira Player Installer** option in the main menu. If you don't require a specific version of the Admira Player, just select de default Player Version: **Install HTML5 version (stable)**

![Default Player Version](docs/img/install-default-version.png)

If you require a specific version of the installer select the **Other versions** entry from the menu. And select the version you need.

![Specific Player Version](docs/img/install-specific-version.png)

Finally the installation process will start showing a screen like this:

![Installation progress](docs/img/installation-progress.png)

When the installation ends just restart the computer using the following **Restart** option from the main menu.


### How to register your player in the Admira Digital Signage Platform

After the reboot, the computer will start showing the registration process screen:

![Installation progress](docs/img/registration-process-1.png)

Wait for a while until the following screen will appear:

![Installation progress](docs/img/registration-process-2.png)

Now you can manage your new player and add content from our digital signage platform on [new.admira.mobi](https://new.admira.mobi).

Enjoy...

### Install Raspberry Pi OS Lite (32bits)

To install the Raspberry Pi OS Lite, first you need to install the **Raspberry Pi Imager** tool. This tool is available for Windows, macOS and Linux. Just grab the installer required by you operative system, install it and then follow these steps:

  * [How to install Raspberry Pi OS](https://www.raspberrypi.com/documentation/computers/getting-started.html#installing-the-operating-system)
  * **IMORTANT!!!** Choose the 32 bits version of the Raspberry Pi OS Lite from the **Operative System** option.
  * **IMPORTANT!!!** During the installation process with Rasperry Pi Imager do not use `admira` as a user name.


### Install Debian

  * Download [debian-11.7.0-i386-netinst.iso](https://cdimage.debian.org/debian-cd/current/i386/iso-cd/debian-11.7.0-i386-netinst.iso) or [debian-11.7.0-amd64-netinst.iso](https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.7.0-amd64-netinst.iso) and install it.
  
    * Tip: You can use the [balenaEtcher](https://www.balena.io/etcher/) tool in order to create a bootable USB flash drive with this image.
  * **IMPORTANT!!!** During the installation process you will be prompted to create a user. Please, do not use `admira` for such a user. Furthermore, select **only** these two packages groups when in the package selection step: `SSH server` and `standard system utilities`.
  * Now follow the steps described in the **How to install ADmira Player** section.
