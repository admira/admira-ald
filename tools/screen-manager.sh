#!/usr/bin/env bash
#
# mmartinez@admira.com
#
# This script allows you to rotate the screen and make the changes persistent
#

function show_apply_permanent_msg {
  (
  for i in 5 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 95 100
  do
    echo $i
    sleep 0.5
  done
  ) | zenity --progress \
    --title="New screen settings" \
    --text="Reverting changes... Press cancel to mantain current settings ?" \
    --percentage=0 \
    --auto-close
  # Don't put any command here!!!
  if [ "$?" = 1 ] ; then
    apply="yes"
  fi
}

function save_orientation {
user=$(whoami)
echo "xrandr -o ${1}" > ~/.fluxbox/screen_orientation
chown $user.$user ~/.fluxbox/screen_orientation
chmod a+x ~/.fluxbox/screen_orientation
}

function save_resolution {
user=$(whoami)
echo "xrandr -s ${1}x${2} -r $3" > ~/.fluxbox/screen_resolution
chown $user.$user ~/.fluxbox/screen_resolution
chmod a+x ~/.fluxbox/screen_resolution
}

function save_mode {
user=$(whoami)
echo "xrandr --output $1 --mode $2" > ~/.fluxbox/screen_resolution
chown $user.$user ~/.fluxbox/screen_resolution
chmod a+x ~/.fluxbox/screen_resolution
}

function auto_fix {
  export DISPLAY=:0.0
  x_res=$1
  y_res=$2
  refresh=$3
  output=$(xrandr -q |grep ' connected'|awk '{print $1}')
  # Detect current orientation
  . $screen_settings
  # Detect output (xrandr)
  output=$(xrandr -q |grep ' connected ' | awk '{print $1}')
  # Get modeline for the resolution (cvt, gtf)
  modeline=$(gtf $x_res $y_res $refresh |grep Modeline | sed -e 's/.*Modeline \".*\"  //')
  # Add new modeline (xrandr)
  cat > $HOME/.fluxbox/custom_modeline <<EOF
xrandr --newmode "admira-ald" $modeline
xrandr --addmode $output "admira-ald"
xrandr --output $output --mode "admira-ald"
EOF
  # Load new settings
  . $HOME/.fluxbox/custom_modeline
}

apply='no'
orientation='normal'
screen_orientation='/home/admira/tools/screen_orientation'
screen_resolution='/home/admira/tools/screen_resolution'

case $1 in
   normal)
     xrandr -o $1
     show_apply_permanent_msg
     if [ "$apply" == "yes" ]; then
        save_orientation $1
        #save_settings
        echo "orientation='normal'" > $screen_orientation
      else
        # load previous value
        . $screen_orientation
        xrandr -o $orientation
     fi
   ;;
   left)
     xrandr -o $1
     show_apply_permanent_msg
     if [ "$apply" == "yes" ]; then
        save_orientation $1
        #save_settings
        echo "orientation='left'" > $screen_orientation
      else
        # load previous value
        . $screen_orientation
        xrandr -o $orientation
     fi
   ;;
   right)
     xrandr -o $1
     show_apply_permanent_msg
     if [ "$apply" == "yes" ]; then
        save_orientation $1
        #save_settings
        echo "orientation='right'" > $screen_orientation
      else
        # load previous value
        . $screen_orientation
        xrandr -o $orientation
     fi
   ;;
   autofix)
     auto_fix $2 $3 $4
   ;;
   size)
     xrandr -s "${2}x${3}" -r $4
     show_apply_permanent_msg
     if [ "$apply" == "yes" ]; then
	      save_resolution $2 $3 $4
        #save_settings
	      echo "resolution='${2}x${3}'" >  $screen_resolution
	      echo "refresh='$4'" >> $screen_resolution
     else
	      # load previous value
	      . $screen_resolution
	      xrandr -s $resolution -r $refresh
     fi
   ;;
   mode)
     output=$(xrandr -q | grep ' connected' | awk '{ print $1 }')
     xrandr --output $output --mode $2
     show_apply_permanent_msg
     if [ "$apply" == "yes" ]; then
	      save_mode $output $2
	      echo "mode='$2'" >  $screen_resolution
	      echo "output='$output'" >> $screen_resolution
     else
	      # load previous value
	      . $screen_resolution
	      xrandr --output $output --mode $mode
     fi
   ;;
   *)
     cat << EOF
usage: screen-manager.sh <normal|left|right|resolution> [x] [y] [refresh]

   normal    : rotate to normal (not rotated at all)
   left      : rotate letf
   right     : rotate right
   autofix   : auto detects the correct modeline for the provided resolution.
               Useful when your screen can't display the desktop automaticaly.
           x : Desired horizontal resolution for your screen.
           y : Desired vertical resolution for your screen.
           r : Refresh rate.
   size      : Sets a different resolution for your desktop.
           x : Desired horizonal resolution for your screen.
           y : Desired vertical resolution for your screen.
           r : Refresh rate.
   mode      : Sets a resolution and a refresh rate using a pre-defined XID
               identifier mode code as shown by xrandr --debug.
         xid : Code mode identified.

   example 1: screen-manager.sh size 1920 1080 60
   example 2: screen-manager.sh mode x5f

EOF
     sleep 2
     exit 1
   ;;
esac
