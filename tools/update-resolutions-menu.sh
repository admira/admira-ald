#!/bin/bash
#
# mmartinez@admira.com
#
# This script detects and create/update the available-resolutions-menu
# The script is executed on every fluxbox startup
#

arm="$HOME/.fluxbox/available-resolutions-menu"

if [ -f $arm ]; then
  rm -f $arm
fi

if [ ! -f "$HOME/tools/screen_resolution" ]; then
  connected_screen_properties=$(xrand --verbose |grep ' connected')
  mode=$(echo $connected_screen_properties | awk '{ print $5 }' | tr -d '()')
  output=$(echo $connected_screen_properties | awk '{ print $1 }')
  echo "mode='$mode'" > $HOME/tools/screen_resolution
  echo "output='$output'" >> $HOME/tools/screen_resolution
fi

> /tmp/.raw-resolutions
> /tmp/.resolutions

xrandr --verbose | grep -v connected |  grep '(0x' -A 2 > /tmp/.raw-resolutions

n=1

while IFS="" read -r line || [ -n "$line" ] 
do
  #printf "%s\n" "$line"
  if grep -q 'x' <<< $line 
  then
    name=$(awk '{ print $1 }' <<< $line)
    mode=$(awk '{ print $2 }' <<< $line)
  fi
  if grep -q 'height' <<< $line
  then
    refresh=$(awk '{ print $11 }' <<< $line)
  fi
  if (( $n % 3 == 0 ))
  then
    echo $name $(echo $mode| tr -d '()') $refresh >> /tmp/.resolutions
  fi
  ((n++))
done < /tmp/.raw-resolutions

previous_res="none"

sort -rn /tmp/.resolutions > /tmp/.sorted-resolutions

while IFS="" read -r line || [ -n "$line" ]
do
  name=$(echo $line|awk '{ print $1 }')
  mode=$(echo $line|awk '{ print $2 }')
  refresh=$(echo $line|awk '{ print $3 }')

  if [ "$previous_res" != "$name" ] && [ "$previous_res" == "none" ]
  then
    echo "[submenu] ($name)" >> $arm
    printf "   [exec] (%s) {/home/admira/tools/screen-manager.sh mode %s}\n" "$refresh" "$mode" >> $arm
  fi

  if [ "$previous_res" != "$name" ] && [ "$previous_res" != "none" ]
  then
    echo "[end]" >> $arm
    echo "[submenu] ($name)" >> $arm
    printf "   [exec] (%s) {/home/admira/tools/screen-manager.sh mode %s}\n" "$refresh" "$mode" >> $arm
  fi

  if [ "$previous_res" == "$name" ]
  then
    printf "   [exec] (%s) {/home/admira/tools/screen-manager.sh mode %s}\n" "$refresh" "$mode" >> $arm
  fi

  previous_res=$name
  ((n++))
done < /tmp/.sorted-resolutions

echo "[end]" >> $arm
